#
# <setup.sh>
#
# Script to install the Weather Research and Forecasting model (WRF), version 3.9.1.1
# on a Linux machine.
#
# Further reading:
# - Official installation guide:
#     https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compilation_tutorial.php
# - [WRF] How to fix the gfortran issue for version 10 and later:
#     https://github.com/SJSU-CS-systems-group/WRF-SFIRE/commit/5c801cb36e2f32fc885b3020c7160c689081220e
# - [WRF] How to fix the libtirpc (or types.h, or landread.c, etc.) issue:
#     https://forum.mmm.ucar.edu/phpBB3/viewtopic.php?t=61
#     https://github.com/wrf-model/WRF/pull/1072/commits/c64512424c5d1741ab290897f473d31edc264966
#     https://github.com/JohnWStockwellJr/SeisUnix/issues/12#issuecomment-416029345
# - [WPS] How to fix
#     https://github.com/lvc0107/wrf_mendieta/blob/master/lista_errores
# - [WPS] How to fix mismatch issues:
#     Same solution for gfortran version 10 but modifying WPS files.
# - [WPS] How to fix IAND related issues:
#     https://github.com/wrf-model/WPS/pull/119

# Define target and patch directories
target_dir=~/.local/opt/Build_WRF
patch_dir=~/docs/src/wrf-install/patches/patch_wps-wrf.diff

# Clean target directory
rm -r $target_dir
mkdir -p $target_dir
cd $target_dir

# Check if some required programs exist
program_exist () {
    if [ ! -z $(which $1 2> /dev/null) ]; then
        echo "$1 exists!"
    else
        echo "$1 does not exist! Please, install." && echo && exit 1
    fi
}

program_exist gfortran
program_exist cpp 
program_exist gcc 
echo

# Check gcc version (it must be $thresh_ver or later) 
thresh_ver=4.6
old_ver=$(echo -e "$(gcc --version | head -n1 | cut -d ' ' -f3)\n$thresh_ver" | sort -V | head -n1)
[ "$thresh_ver" != "$old_ver" ] && echo "gcc version must be $thresh_ver or later!" && exit 1
echo

# some fortran and c tests
mkdir TESTS && cd TESTS
curl -O https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/Fortran_C_tests.tar
echo
tar -xf Fortran_C_tests.tar
rm Fortran_C_tests.tar

gfortran TEST_1_fortran_only_fixed.f 2> TEST_1_fortran_only_fixed.log || echo "TEST_1_fortran_only_fixed.f compilation failed!" || exit 1
./a.out
gfortran TEST_2_fortran_only_free.f90 2> TEST_2_fortran_only_free.log || echo "TEST_2_fortran_only_free.f90 compilation failed!" || exit 1
./a.out
gcc TEST_3_c_only.c 2> TEST_3_c_only.log || echo "TEST_3_c_only.c compilation failed!" || exit 1
./a.out
gcc -c -m64 TEST_4_fortran+c_c.c 2> TEST_4_fortran+c_c.log || echo "TEST_4_fortran+c_c.c compilation failed!" || exit 1
./a.out
gfortran -c -m64 TEST_4_fortran+c_f.f90 2> TEST_4_fortran+c_f.log || echo "TEST_4_fortran+c_f.f90 compilation failed!" || exit 1
./a.out
gfortran -m64 TEST_4_fortran+c_f.o TEST_4_fortran+c_c.o 2> TEST_4_fotran+c.log || echo "TEST_4_fotran+c compilation failed!" || exit 1
./a.out
echo

# do shells and perl exist?
program_exist csh
./TEST_csh.csh
program_exist perl 
./TEST_perl.pl
program_exist sh 
./TEST_sh.sh
echo

# libraries
cd ..
mkdir -p LIBRARIES
cd LIBRARIES

export DIR=$target_dir/LIBRARIES
export CC=gcc
export CXX=g++
export FC=gfortran
export FCGLAGS=-m64
export F77=gfortran
export FFLAGS="-m64 -fallow-argument-mismatch"
export JASPERLIB=$DIR/grib2/lib
export JASPERINC=$DIR/grib2/include
export LDFLAGS=-L$DIR/grib2/lib
export CPPFLAGS=-I$DIR/grib2/include

# build netcdf
curl -O https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/netcdf-4.1.3.tar.gz
tar -xzvf netcdf-4.1.3.tar.gz
rm netcdf-4.1.3.tar.gz
cd netcdf-4.1.3

./configure --prefix=$DIR/netcdf --disable-dap --disable-netcdf-4 --disable-shared
make
make install
export PATH=$DIR/netcdf/bin:$PATH
export NETCDF=$DIR/netcdf
cd ..
rm -r netcdf-4.1.3
echo

# build mpich
curl -O https://www.mpich.org/static/downloads/3.4.2/mpich-3.4.2.tar.gz 
tar -xzvf mpich-3.4.2.tar.gz
rm mpich-3.4.2.tar.gz
cd mpich-3.4.2
./configure --prefix=$DIR/mpich \
    --with-device=ch3
make
make install
export PATH=$DIR/mpich/bin:$PATH
cd ..
rm -r mpich-3.4.2

# build zlib
curl -O https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/zlib-1.2.7.tar.gz
tar -xzvf zlib-1.2.7.tar.gz
rm zlib-1.2.7.tar.gz
cd zlib-1.2.7
./configure --prefix=$DIR/grib2
make
make install
cd ..
rm -r zlib-1.2.7

# build libpng
curl -O https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/libpng-1.2.50.tar.gz
tar -xzvf libpng-1.2.50.tar.gz
rm libpng-1.2.50.tar.gz
cd libpng-1.2.50
./configure --prefix=$DIR/grib2
make
make install
cd ..
rm -r libpng-1.2.50

# build jasper
curl -O https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/jasper-1.900.1.tar.gz
tar -xzvf jasper-1.900.1.tar.gz     #or just .tar if no .gz present
rm jasper-1.900.1.tar.gz
cd jasper-1.900.1
./configure --prefix=$DIR/grib2
make
make install 
cd ..
rm -r jasper-1.900.1
echo

# tests
cd ../TESTS
curl -O https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/Fortran_C_NETCDF_MPI_tests.tar
tar -xf Fortran_C_NETCDF_MPI_tests.tar
rm Fortran_C_NETCDF_MPI_tests.tar

# test 1
cp ${NETCDF}/include/netcdf.inc .
gfortran -c 01_fortran+c+netcdf_f.f
gcc -c 01_fortran+c+netcdf_c.c
gfortran 01_fortran+c+netcdf_f.o 01_fortran+c+netcdf_c.o \
     -L${NETCDF}/lib -lnetcdff -lnetcdf
./a.out

# test 2
cp ${NETCDF}/include/netcdf.inc .
mpif90 -c 02_fortran+c+netcdf+mpi_f.f
mpicc -c 02_fortran+c+netcdf+mpi_c.c
mpif90 02_fortran+c+netcdf+mpi_f.o \
02_fortran+c+netcdf+mpi_c.o \
     -L${NETCDF}/lib -lnetcdff -lnetcdf
mpirun ./a.out
cd ..
echo

# download wps and wrf
curl -O https://www2.mmm.ucar.edu/wrf/src/WPSV3.9.1.TAR.gz
tar -xvzf WPSV3.9.1.TAR.gz && rm WPSV3.9.1.TAR.gz
curl -O https://www2.mmm.ucar.edu/wrf/src/WRFV3.9.1.1.TAR.gz
tar -xvzf WRFV3.9.1.1.TAR.gz && rm WRFV3.9.1.1.TAR.gz

# apply patch
ln -s $patch_dir .
patch -p1 < patch_wps-wrf.diff

# build wrf
cd WRFV3
./configure
echo
case_name="em_real"
echo "Compiling $case_name..."
echo "Take a seat and wait about 20-30 minutes..."
./compile em_real >& log.compile
echo

# WPS installation
cd ../WPS
./configure
./compile >& log.compile
