#
# <header.sh>
#

echo "             ______________________________________________________"
echo "           ||              __        ______  _____                 ||"
echo "           ||              \ \      / /  _ \|  ___|                ||"
echo "           ||               \ \ /\ / /| |_) | |_                   ||"
echo "           ||                \ V  V / |  _ <|  _|                  ||"
echo "           ||                 \_/\_/  |_| \_\_|                    ||"
echo "           ||   _           _        _ _       _   _               ||"
echo "           ||  (_)_ __  ___| |_ __ _| | | __ _| |_(_) ___  _ __    ||" 
echo "           ||  | | '_ \/ __| __/ _' | | |/ _' | __| |/ _ \| '_ \   ||"
echo "           ||  | | | | \__ \ || (_| | | | (_| | |_| | (_) | | | |  ||"
echo "           ||  |_|_| |_|___/\__\__,_|_|_|\__,_|\__|_|\___/|_| |_|  ||"
echo "           ||______________________________________________________||"
echo
echo
echo "  ATTENTION:"
echo "      The installation process is not completely automatic."
echo "      You will have to manually choose among different options"
echo "      at the beginning of both WRF and WPS compilation steps."
echo
echo "      Press any key to start the installation..."
